import React from 'react';

const Login = () => (
    <div>
        <p>You're not logged in.</p>
        <button>Login</button>
    </div>
);

export default Login;
