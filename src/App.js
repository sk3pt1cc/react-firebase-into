import React from 'react';
import LoggedIn from './components/LoggedIn';
import Login from './components/Login';

function App() {
  const loggedIn = false;
  return (
    <div className="App">
      {loggedIn ? <LoggedIn /> : <Login />}
    </div>
  );
}

export default App;
